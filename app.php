<?php
spl_autoload_register(function($class_name) 
{
    require_once('class/'. $class_name . '.php');
});
$db = new Connect();
$connecting = $db->getConnection();

if(isset($_POST['send'])) {
    $msg = $_POST['message'];
    $sql = "INSERT INTO user_messages (name, message) VALUES ('rahul','$msg')";
    mysqli_query($connecting, $sql);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Messanger App</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <div>
            <table style="border-spacing: 15px;">
                <thead>
                    <tr>
                        <th>My Messages</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $myMessage = "SELECT message FROM user_messages WHERE name = 'atul';";
                        $chat = mysqli_query($connecting, $myMessage);
                        while($chatRow = $chat->fetch_assoc()) {
                    ?>
                    <tr>
                        <td><?php echo $chatRow['message'];?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <form action="" method="POST" class="app-form">
                <div>
                    <input type="text" name="message" class="messaging">
                </div>
                    <input type="submit" name="send" value="Send">
            </form>
            <p>Click here to <a href="index.php">Log Out</a> from your account</p>
        </div>
    </body>
</html>

