<?php
spl_autoload_register(function($class_name) 
{
    require_once('class/'. $class_name . '.php');
});

$db = new Connect();
$connecting = $db->getConnection();
if(isset($_POST['register'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $dob = $_POST['dob'];
    $enterArray = [
            "name" => $name,
            "email" => $email,
            "dob" => $dob
        ];
    $validate = new Validation();
    $validate->insertUser($enterArray);

}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>User Registration</title>
        <link href="style.css" type="stylesheet">
    </head>
    <body>
        <form action="" method="POST">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div>
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="" placeholder="Enter your Name">
                <span></span>
            </div>
            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="" placeholder="Enter your Email-id">
            </div>
            <div>
                <label for="dob">DOB</label>    
                <input type="date" name="dob" id="dob" value="" placeholder="Enter your DOB">
            </div>
            <input type="submit" name="register" id="submit">
        </form>  
        <p>Already a user click <a href="index.php">here</a> to login</p>
    </body>
</html>