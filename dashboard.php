<?php
// session_start();
spl_autoload_register(function($class_name) 
{
    require_once('class/'. $class_name . '.php');
});
// check if user is already logged in
    // if(isset($_SESSION['email'])){
    //     header('location:dashboard.php');
    // }

$db = new Connect();
$connecting = $db->getConnection();

if(isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $edit_state = true;
    $rec = mysqli_query($connecting, "SELECT * FROM users_details WHERE id=$id");
    $record = mysqli_fetch_array($rec);
    $name = $record['name'];
    $id = $record['id'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Messanger App</title>
        <link href="style.css" type="stylesheet">
    </head>
    <body>
    <table style="border-spacing: 15px;">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date of Birth</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $selectQuery = "SELECT * FROM users_details;";
                    $result = mysqli_query($connecting, $selectQuery);
                    while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                
                    <td><?php echo $row['name'];?></td>
                    <td><?php echo $row['email'];?></td>
                    <td><?php echo $row['dob'];?></td>
                    <td>
                        <a href="register.php?edit=" class="editing">Edit</a>
                    </td>
                    <td>
                        <a href="index.php?del=" class="delete">Delete</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </body>
</html>