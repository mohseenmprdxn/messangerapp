<?php
spl_autoload_register(function($class_name) 
{
    require_once('class/'. $class_name . '.php');
});

$db = new Connect();
$connection = $db->getConnection();
if (isset($_GET['inserted'])) {
    echo "<script>alert('New user registered')</script>";
}
// login verification
if(isset($_POST['login'])) {
    // $userName = $_POST['name'];
    // $userEmail = $_POST['email'];
    // $loginArray= [
        //     'name' => $userName,
        //     'email' => $userEmail,
        // ];
    $verifying = new Authentication();
    $verifying->checkUser();
    // header('location:app.php');
}


?>

<!DOCTYPE html>
<html>
    <head>
        <title>Messanger App</title>
        <link href="style.css" type="stylesheet">
    </head>
    <body>
        <form action="" method="POST">
            <div>
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="" placeholder="Enter your Name">
            </div>
            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="" placeholder="Enter your Email-id">
            </div>
            <input type="submit" name="login" id="login">
        </form>  
        <p>New User? <a href="register.php">Register here</a></p>
    </body>
</html>